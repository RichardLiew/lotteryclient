/**
 * 绑定银行卡
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edCardNumber:{
            default: null,
            type: cc.EditBox
        },

        labOpenBank:{
            default: null,
            type: cc.Label
        },

        // swOpenBank:{
        //     default: null,
        //     type: cc.ScrollView
        // },

        edCode:{
            default: null,
            type: cc.EditBox
        },

        bankNameItem:{
            default: null,
            type: cc.Prefab
        },
        labDec:{
            default:null,
            type:cc.Label
        },

        _bgetVerity:true,
        callback:null,
        bankContent:null,
        intervalTime: 60,
        bankCardNum:"",
        verify:"",
    },

    // use this for initialization
    onLoad: function () {
        this.labOpenBank.string == "";
        this.bankCardNum = "";
    },

    onSelectBank:function(){
    },

    onClickCallBack:function(event, customEventData){
        this.labOpenBank.string = customEventData;
    },

    init:function(data){
        this.callback = data;
    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.intervalTime = 60;
            this.labDec.string = "获取验证码";
        }
    },

     onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.verify = txt;
        else
        {
            txt = this.verify;
            editbox.string = txt;
            return;
        }
     },

    onTextChangedCard:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.bankCardNum = txt;
        else
        {
            txt = this.bankCardNum;
            editbox.string = txt;
            return;
        }
        
       // cc.log("onTextChanged:"+text);
        
        if(txt.length >= 15)
        {
             var recv = function(err, bin){
                if(err == null)
                {
                   if(bin != "")
                    {
                        if(bin.cardType == "DC")
                        {
                             this.labOpenBank.string = bin.bankName;
                        }
                        else
                        {
                            ComponentsUtils.showTips("目前只支持绑定储蓄卡！");
                        }
                    } 
                }
                else
                {
                   this.labOpenBank.string = "";
                }
            }
            LotteryUtils.getBankBin(txt,recv.bind(this));
        }
        else
        {
            this.labOpenBank.string = "";
        }
    },

    checkInput:function(){
        if(this.edCardNumber.string != "")
        {
            if(this.labOpenBank.string == "")
            {
                ComponentsUtils.showTips("请输入有效银行卡号");
                return;
            }
        }
        else
        {
            ComponentsUtils.showTips("银行卡不能为空！");
            return false;
        }
   
        if(this.edCode.string != "")
        {
            if(!Utils.isVerify(this.edCode.string))
            {
                ComponentsUtils.showTips("验证码格式不对！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("验证码不能为空！");
            return false;
        }
        return true;
    },

    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;
        if(this.edCardNumber.string != "")
        {
            if(this.labOpenBank.string == "")
            {
                ComponentsUtils.showTips("请输入有效银行卡号");
                return;
            }
        }
        else
        {
            ComponentsUtils.showTips("银行卡不能为空！");
            return;
        }
        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");   
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        cc.log("发送token"+User.getLoginToken());
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: User.getTel(),
            VerifyType:DEFINE.SHORT_MESSAGE.BINDBANK,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
        
    },

    onTrue: function(){
        if(this.labOpenBank.string == "")
        {
            ComponentsUtils.showTips("请输入有效银行卡号");
            return;
        }
         if(this.checkInput())
         {
            var recv = function(ret){
                ComponentsUtils.unblock();
                if(ret.Code == 0)
                {
                    ComponentsUtils.showTips("绑定银行卡成功！");
                    this.callback("");
                    this.onClose();
                }
                else
                {
                        ComponentsUtils.showTips(ret.Msg);
                }
            }.bind(this);

            var data = {
                Token: User.getLoginToken(),
                UserCode: User.getUserCode(),
                BankName: this.labOpenBank.string,
                CardNum: this.edCardNumber.string,
                Area: "",
                Mobile:User.getTel(),
                VerifyCode:this.edCode.string,
            };
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.BINDBANKCARD, data, recv.bind(this),"POST");  
            ComponentsUtils.block();   
         }
    },

    onClose: function(){
        this.node.getComponent("Page").backAndRemove();
    },
});

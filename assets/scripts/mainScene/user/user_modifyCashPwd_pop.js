/**
 * 重置密码界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labNumber:{
            default: null,
            type: cc.Label
        },

        labTopTitle:{
            default: null,
            type: cc.Label
        },

        edCode:{
            default: null,
            type: cc.EditBox
        },

        modifyCashSetPwdPrefab: {
            default: null,
            type: cc.Prefab
        },

        labDec:{
            default:null,
            type:cc.Label
        },

        intervalTime: 60,

        _bgetVerity:true,
        _tel:"",
        _type:0,
        _verify:""
    },

    // use this for initialization
    onLoad: function () {
        this._tel = User.getTel();
        if(this._tel != "")
        {
            var top = "*******";
            var down = this._tel.substring(7,11);
            this.labNumber.string = top+down;
        }
        this._verify = "";
    },

    init:function(data){
        this._type = data;
        if(this._type == 1)//登录密码修改
        {
            this.labTopTitle.string = "修改登录密码";
        }
        else if(this._type == 2)//提现密码修改
        {
            this.labTopTitle.string = "修改提现密码";
        }
    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this.intervalTime = 60;
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.labDec.string = "获取验证码"
        }
    },

    onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this._verify = txt;
        else
        {
            txt = this._verify;
            editbox.string = txt;
            return;
        }
     },

    checkInput:function(){
        if(this.edCode != "")
        {
            if(!Utils.isVerify(this.edCode.string))
            {
                ComponentsUtils.showTips("验证码格式不对！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("验证码不能为空！");
            return false;
        }
        return true;
    },

    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;

        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                cc.error(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");   
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: this._tel,
            VerifyType:this._type==1? DEFINE.SHORT_MESSAGE.CHANGEPWD:DEFINE.SHORT_MESSAGE.CHANGEPAY,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onNext: function(){
     if(this.checkInput())
        {
            var recv = function(ret){
                ComponentsUtils.unblock();
                if (ret.Code !== 0) {
                    cc.error(ret.Msg);
                    ComponentsUtils.showTips(ret.Msg);
                }else{    
                   
                     var canvas = cc.find("Canvas");
                     var modifyCashSetPwdPrefab = cc.instantiate(this.modifyCashSetPwdPrefab);
                    if(this._type == 1)//登录密码修改
                   {
                        modifyCashSetPwdPrefab.getComponent("user_modifyCashSetPwd_pop").init({
                             Code:this.edCode.string,
                             type:this._type,
                        });
                   }
                   else if(this._type == 2)//提现密码修改
                   {
                        modifyCashSetPwdPrefab.getComponent("user_modifyCashSetPwd_pop").init({
                             Code:this.edCode.string,
                             type:this._type,
                        });
                   }
                    canvas.addChild(modifyCashSetPwdPrefab);
                    modifyCashSetPwdPrefab.active = true;
                    this.onClose();
                }
            }.bind(this);

            var data = {
                Token: User.getLoginToken(),
                Mobile: this._tel,
                VerifyCode: this.edCode.string,
                VerifyType:this._type==1? DEFINE.SHORT_MESSAGE.CHANGEPWD:DEFINE.SHORT_MESSAGE.CHANGEPAY,
            };
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.CHECKVERIFY, data, recv.bind(this),"POST");  
            ComponentsUtils.block();
        }
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    },
});

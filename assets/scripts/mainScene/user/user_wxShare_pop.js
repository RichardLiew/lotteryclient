/**
 * 分享界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
            console.log('user_wxShare node  TOUCH_END');
            var contentRect = this.node.getChildByName("content").getChildByName("spSelectBg").getBoundingBoxToWorld();
            var touchLocation = event.getLocation();
            if(cc.rectContainsPoint(contentRect, touchLocation) == false){
            //    cc.log("user_wxShare close");
                this.onClose();
            }
        }, this);
    },

    //分享 微信好友/微信朋友圈
    onWxShare:function(event,customEventData){
        ComponentsUtils.showTips("暂时不支持微信分享！");
    },

    /** 
    * 接收分享数据信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    }
    
});

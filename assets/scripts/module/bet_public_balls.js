cc.Class({
    extends: cc.Component,

    properties: {
        spriteList:{
            default:[],
            type:cc.Sprite
        },
        
        lablist:{
            default:[],
            type:cc.Label
        },

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null) 
        {
            if(this._data.spNums != null && this.spriteList != null)
            {
                for(var i=0;i<this._data.spNums.length;i++)
                {
                    this.spriteList[i].spriteFrame = this._data.spNums[i];
                    this.spriteList[i].node.scale = 0.9;
                }
            }
            if(this._data.nums != null && this.lablist != null)
            {
                for(var i =0; i<this._data.nums.length;i++)
                {
                    this.lablist[i].string = this._data.nums[i] == "" ?"?":this._data.nums[i];
                }
            }
        }
    },

    init: function(ret){
        this._data = ret;
    },

    updataData:function(data){
        this._data = data;
        if(this._data != null) 
        {
            if(this._data.spNums != null && this.spriteList != null)
            {
                for(var i=0;i<this._data.spNums.length;i++)
                {
                    this.spriteList[i].spriteFrame = this._data.spNums[i];
                }
            }
            if(this._data.nums != null && this.lablist != null)
            {
                for(var i =0; i<this._data.nums.length;i++)
                {
                    this.lablist[i].string = this._data.nums[i] == "" ?"?":this._data.nums[i];
                }
            }
        }
    },

});

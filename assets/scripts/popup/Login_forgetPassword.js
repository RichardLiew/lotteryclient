/**
 * 找回密码
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edAccount: {
            default: null,
            type: cc.EditBox
        },
        edPassword: {
            default: null,
            type: cc.EditBox
        },
        labDec:{
            default:null,
            type:cc.Label
        },

        _bgetVerity:true,
        intervalTime: 60
    },

    // use this for initialization
    onLoad: function () {

    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this.intervalTime = 60;
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.labDec.string = "获取验证码"
        }
    },

    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;

        if(this.edAccount.string == "")
        {
            ComponentsUtils.showTips("请输入手机号");
            return;
        }   

        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                cc.error(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");   
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: this.edAccount.string,
            VerifyType:DEFINE.SHORT_MESSAGE.FINDPWD,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onTextChangedPhone:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.phoneNum = txt;
        else
        {
            txt = this.phoneNum;
            editbox.string = txt;
            return;
        }
     },

    onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.verify = txt;
        else
        {
            txt = this.verify;
            editbox.string = txt;
            return;
        }
    },

    onNext: function(){
        if(Utils.checkInput(null,null,this.edPassword.string,null,true))
        {
            var data = {
                account:this.edAccount.string,
                password:this.edPassword.string,
            }
            window.Notification.emit("onResetPassword",data);
            this.onClose();
        }
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }
});

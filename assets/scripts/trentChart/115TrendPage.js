cc.Class({
    extends: cc.Component,

    properties: {
        rtEndTime:{
            default:null,
            type:cc.RichText
        },

        //选择玩法按钮
        spArrow:{
            default:null,
            type:cc.Node
        },

        //面板控制
        tgSelRule:{
            default:null,
            type:cc.Toggle
        },

        ndRulePanel:{
            default:[],
            type:cc.Node
        },


        labName:{
            default:null,
            type:cc.Label
        },

        spBg:{
            default:null,
            type:cc.Node
        },

        _curPanle:-1,
        _panles:[],
    },

    // use this for initialization
    onLoad: function () {
        this._curPanle = -1;
        this.getServerData(this._data.playCode,0);
    },

    getServerData:function(code,index){
        var self = this;
        if(this._panles[index] == 0)
        {
            TrentChart.initTrentChart(this._data.Lotteryid,code,function(data){
                self.showPage(data,index);
            });
        }
    },

    selRulePanelTg:function(toggle){
        this.setSelRuleVisible(toggle.getComponent(cc.Toggle).isChecked);
    },

    setSelRuleVisible:function(isvisible){
            this.tgSelRule.isChecked = isvisible;
            this.spArrow.rotation = isvisible?180:0;
    },

    selRuleTg:function(toggle,customEventData){
        if(this._curPanle == customEventData)
        {
            toggle.getComponent(cc.Toggle).isChecked = true;
            this.labName.string = toggle.node.getChildByName("labDec").getComponent(cc.Label).string;
            this.setSelRuleVisible(false);
            return;
        }

        this.setSelRuleVisible(false);
        var code = this.getPanleInfo(customEventData);
        this.labName.string = toggle.node.getChildByName("labDec").getComponent(cc.Label).string;
        if(code == -1)
            return;
        this.getServerData(code,customEventData);
    },

    getPanleInfo:function(type){
        var playercode = -1;

        switch (type)
        {
            case "0":
            {
                playercode = this._data.Lotteryid + "01";
            }
            break;
            case "1":
            {
                playercode = this._data.Lotteryid + "08";
            }
            break;
            case "2":
            {
                playercode = this._data.Lotteryid + "09";
            }
            break;
            case "3":
            {
                playercode = this._data.Lotteryid + "11";
            }
            break;

        }
        return playercode;
    },

    showPage:function(info,index){
        if(this.ndRulePanel[index] != null)
        {
            this.ndRulePanel[index].getComponent(this.ndRulePanel[index].name).init(info);
            this.ndRulePanel[index].active = true;
            if(this._curPanle != -1)
                this.ndRulePanel[this._curPanle].active = false;
            
            this._curPanle = index;  

            this._panles[index] = 1;
        }
        else
        {
            if(this._curPanle != -1)
                this.ndRulePanel[this._curPanle].active = false;
        }
    },

    init:function(data,home){
        this._data = data;
        this._home = home;
        this._panles = [0,0,0,0];
    },

    update:function(dt){
        var issueStr = TrentChart.getCurIssue();
        if(issueStr != "")
        {
            var currentTimeStamp = Date.parse(new Date())/1000;
            var time = 0;
            var endtime = TrentChart.getEndTime();
            if(endtime == "91")
            {
                this.rtEndTime.string = "<color=#000000>本期已封盘</c>";
                return;
            } 
            else if(endtime == "90")
            {
                this.rtEndTime.string == "<color=#000000>预售中</c>"; 
                return;
            }
            var leftTimeStamp = endtime - currentTimeStamp;
            if(leftTimeStamp >= 0){
                time = endtime - currentTimeStamp;
            }
            else
            {
                
                this.rtEndTime.string = "<color=#000000>距"+ issueStr +"期投注截止：</c><color=#ff0000>--:--:--</color>";
                return;
            }
            var h = parseInt(time/(60*60)).toString();
            var hStr = (parseInt(h)%24).toString();
            var dStr = parseInt( parseInt(h)/24).toString();
            hStr = hStr.length==1?("0"+hStr):hStr;
            var m = (parseInt((time-h*60*60)/60)).toString();
            m = m.length==1?("0"+m):m;
            var s = (parseInt(time-h*60*60-m*60)).toString();
            s = s.length==1?("0"+s):s;
            var timeStr = "";
            if(parseInt(dStr)>0){
                timeStr = dStr+"天 "+ hStr+":"+m+":"+s;
            }else{
                timeStr = hStr+":"+m+":"+ s;
            }
            this.rtEndTime.string = "<color=#000000>距"+ issueStr +"期投注截止：</c><color=#ff0000>"+ timeStr +"</color>";
        }
        else
               this.rtEndTime.string = "";
    },

    onClose:function(){
         this.node.getComponent("Page").backAndRemove();
          //父节点显示
          this._home.node.active =true;
    },
});
